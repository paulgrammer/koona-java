package com.pjm.koona.controllers;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CommunityTab implements Initializable{

    @FXML VBox messagesContainer;

    @Override
    public void initialize(URL location, ResourceBundle resources){

        Node[] nodes = new Node[20];

        for(int i = 0; i < nodes.length; i++){
            try {
                nodes[i] = FXMLLoader.load(getClass().getResource("/fxml/components/chat-bubble-text-message.fxml"));
                messagesContainer.getChildren().add(nodes[i]);
            } catch (IOException e){ }
        }

    }

}
