package com.pjm.koona.controllers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import com.pjm.koona.utils.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import org.json.JSONException;
import org.json.JSONObject;
import com.pjm.koona.fxrouter.FXRouter;
import com.pjm.koona.utils.ClientInfor;
import net.sf.corn.converter.json.JsTypeComplex;

public class Auth extends ClientServerApi implements Initializable {

    @FXML private TextField userEmail;

    @FXML private PasswordField userPassword;

    @FXML private TextField userName;

    @FXML private TextField userPhone;

    @FXML private Label copyRightLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources){

        copyRightLabel.setText(Constants.COPY_RIGHT_FLAG);
        //connect to server
        this.AuthConnect();

    }

    @FXML private void RouteToLogin(ActionEvent event){
        try {
            FXRouter.goTo("login");
        }catch (IOException e){ }
    }

    @FXML private void RouteToSignup(ActionEvent event){
        try {
            FXRouter.goTo("signup");
        }catch (IOException e){ }
    }

    @FXML private void RouteToForgotPassword(ActionEvent event){
        try {
            FXRouter.goTo("forgot_password");
        }catch (IOException e){ }
    }

    @FXML private void LoginNow(ActionEvent event){

        String clientEmail = userEmail.getText();
        String clientPassword = userPassword.getText();
        JSONObject obj = new JSONObject();

        if(!this.AuthServerFailedToConnect()) {
            if (!userEmail.getText().isEmpty()) {

                try {

                    obj.put("email", clientEmail);
                    obj.put("password", clientPassword);

                } catch (JSONException e) {
                }
                this.Login(obj);
            }
        } else{
            System.out.println("failed to connect to server");
        }

    }
    @FXML private void SignUpNow(ActionEvent event){
        JsTypeComplex geo = ClientInfor.getCLIENT_GEO();
        System.out.println(geo);
        String clientEmail = userEmail.getText(),
                clientPhone = userPhone.getText(),
                clientUsername = userName.getText(),
                clientPassword = userPassword.getText();

        JSONObject obj = new JSONObject();

        if(!this.AuthServerFailedToConnect()){

            if(!userEmail.getText().isEmpty()) {
                try {

                    obj.put("email", clientEmail)
                            .put("password", clientPassword)
                            .put("phone", clientPhone)
                            .put("userName", clientUsername);

                } catch (JSONException e) { }

                this.Signup(obj);
            }

        } else {
            System.out.println("failed to connect to server");
        }
    }

    @FXML private void RecoverPassword(ActionEvent event){

    }
}
