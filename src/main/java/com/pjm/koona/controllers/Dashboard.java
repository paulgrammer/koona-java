package com.pjm.koona.controllers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Dashboard implements Initializable {

    @FXML private VBox SideBarLeft;

    @FXML private HBox MainContent;

    private Button TabButtonActive;

    @FXML private Button browse, library, videos, community, news, search;

    @FXML private void OpenLeftPanel(ActionEvent event){
        if(SideBarLeft.getStyleClass().contains("collapse")) {
            SideBarLeft.getStyleClass().remove("collapse");
        } else{
            SideBarLeft.getStyleClass().add("collapse");
        }
    }

    @FXML private void SwitchTab(ActionEvent event){

        Button el = (Button) event.getSource();
        TabButtonActive.getStyleClass().remove("active");
        el.getStyleClass().add("active");
        if(event.getSource() == browse){
            this.LoadFxml("/fxml/browse.fxml", browse);
        } else if(event.getSource() == library){
            this.LoadFxml("/fxml/library.fxml", library);
        } else if(event.getSource() == videos){
            this.LoadFxml("/fxml/videos.fxml", videos);
        } else if(event.getSource() == community){
            this.LoadFxml("/fxml/community.fxml", community);
        } else if(event.getSource() == news){
            this.LoadFxml("/fxml/news.fxml", news);
        } else if(event.getSource() == search){
            this.LoadFxml("/fxml/search.fxml", search);
        } else { }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources){
        this.LoadFxml("/fxml/search.fxml", browse);
    }
    private void LoadFxml(String path, Button button){
        try {
            BorderPane tab = FXMLLoader.load(getClass().getResource(path));
            MainContent.getChildren().setAll(tab);
            TabButtonActive = button;
        }catch (IOException e){}
    }
}

