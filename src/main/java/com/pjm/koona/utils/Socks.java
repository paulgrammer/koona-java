package com.pjm.koona.utils;

import io.socket.client.IO;
import io.socket.client.Socket;
import java.net.URISyntaxException;

public class Socks {
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(Constants.MAIN_SERVER);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}
