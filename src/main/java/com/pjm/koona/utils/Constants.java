package com.pjm.koona.utils;

public class Constants {
    public static final String APP_NAME = "music";
    public static final String COPY_RIGHT_FLAG = "(c) PjM Tech Inc. All Rights Reserved";
    public static final String MAIN_SERVER = "http://localhost:1200";
    public static final String GEO_PLUGIN_SERVER = "http://www.geoplugin.net/json.gp";
    public static final Double WINDOW_WIDTH = 1024.0;
    public static final Double WINDOW_HEIGHT = 600.0;
}
