package com.pjm.koona.utils;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class Routes {

    public static JSONArray getRoutes(){

        JSONArray array = new JSONArray();

        array.put(Build("login", "login.fxml", "Koona", 400, 550));

        return array;
    }

    private static JSONObject Build(String label, String path, String title, double width, double height){

        JSONObject obj = new JSONObject();

        try {
            obj.put("routeLabel", label);
            obj.put("scenePath", path);
            obj.put("winTitle", title);
            obj.put("sceneWidth", width);
            obj.put("sceneHeight", height);
        }catch (JSONException e){ }

        return obj;
    }
}

