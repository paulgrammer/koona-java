package com.pjm.koona.utils;

import net.sf.corn.converter.json.JsTypeComplex;
import net.sf.corn.converter.json.JsonStringParser;

import net.sf.corn.httpclient.HttpClient;
import net.sf.corn.httpclient.HttpResponse;
import net.sf.corn.converter.ParsingException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static com.pjm.koona.utils.Constants.GEO_PLUGIN_SERVER;

public class ClientInfor {

    private static JsTypeComplex CLIENT_GEO = null;

    public static void init(){

        try{
            HttpClient geoplugin = new HttpClient(new URI(GEO_PLUGIN_SERVER));
            HttpResponse Response = geoplugin.sendData(HttpClient.HTTP_METHOD.GET);
            String JsonString= Response.getData();
            CLIENT_GEO  = (JsTypeComplex) JsonStringParser.parseJsonString(JsonString);
        } catch (URISyntaxException | IOException | ParsingException e){ }


    }

    public static JsTypeComplex getCLIENT_GEO() {
        return CLIENT_GEO;
    }
}
