package com.pjm.koona.utils;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class Responsive {
    
    Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

    private Double ScreenWidth(){
        return screenBounds.getWidth();
    }

    private Double ScreenHeight(){
        return screenBounds.getHeight();
    }
}
