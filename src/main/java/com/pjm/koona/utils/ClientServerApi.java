package com.pjm.koona.utils;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import java.net.URISyntaxException;
import org.json.JSONException;
import org.json.JSONObject;


public class ClientServerApi {

    private Socket socket;

    private Socket auth;

    private boolean isConnected = false;

    private boolean isAuthConnected = false;

    private boolean authServerFailure = false;
    {
        try {

            //data path
            IO.Options opts = new IO.Options();
            opts.forceNew = true;
            opts.reconnection = true;
            opts.path = "/data";
            //opts.query = "auth_token=" + authToken;
            socket = IO.socket(Constants.MAIN_SERVER, opts);

            //auth path
            IO.Options authOpts = new IO.Options();
            authOpts.path = "/auth";
            authOpts.forceNew = false;
            authOpts.reconnection = true;
            auth = IO.socket(Constants.MAIN_SERVER, authOpts);

            this.init();

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);
        }
    }

    public void Login(JSONObject data){

        if(isAuthConnected) auth.emit("client:login", data);
    }

    public void Signup(JSONObject data){

        if(isAuthConnected) auth.emit("client:signup", data);
    }
    private void init(){
        auth.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if(!isAuthConnected) {
                    isAuthConnected = true;
                    authServerFailure = false;
                }
            }
        }).on("client:login:success", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                JSONObject obj;
                try {
                    obj = data.getJSONObject("data");
                    System.out.println(obj);
                } catch (JSONException e) {
                    return;
                }
            }
        }).on("client:not:found", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                //client not registered
                System.out.println("client not registered");
            }
        }).on("client:password:incorrect", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                //client password is incorrect
            }
        }).on("client:email:or:phone:exists", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                System.out.println("Email or Phone already exists");

            }
        }).on("client:signup:success", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                System.out.println("User succesfully Register");

            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                isAuthConnected = false;
                authServerFailure = false;
            }
        }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                authServerFailure = true;
            }
        }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                authServerFailure = true;
            }
        }).on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if(!isConnected) {
                    isConnected = true;
                }
            }

        }).on("server", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                JSONObject obj;
                String email;
                try {
                    obj = data.getJSONObject("data");
                    email = obj.getString("email");
                } catch (JSONException e) {
                    return;
                }
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                isConnected = false;
            }

        });
        socket.connect();

    }
    public void AuthConnect(){
        if(!isAuthConnected) auth.connect();
    }
    public boolean AuthServerFailedToConnect(){
        return authServerFailure;
    }
    public void AuthDisconnect(){
        auth.disconnect();
    }
}


