package com.pjm.koona;

import javafx.application.Application;
import javafx.stage.Stage;
import com.pjm.koona.fxrouter.FXRouter;


public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXRouter.bind(this, primaryStage);
		FXRouter.when("dashboard", "dashboard.fxml", 1040,650);
		FXRouter.when("login", "login.fxml",  400, 550);
		FXRouter.when("signup", "signup.fxml",  400, 550);
		FXRouter.when("forgot_password", "forgot_password.fxml", 400, 550);
		FXRouter.goTo("dashboard");
	}

	public static void main(String[] args) {
		launch(args);
	}
}